package com.angel.rdz.arkusnexustest.ui.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.angel.rdz.arkusnexustest.R
import com.angel.rdz.arkusnexustest.ui.home.model.MyGoogleResponse
import com.angel.rdz.arkusnexustest.ui.home.model.Place
import com.angel.rdz.arkusnexustest.utils.ApiManager
import com.angel.rdz.arkusnexustest.utils.GPSTracker
import com.angel.rdz.arkusnexustest.utils.RetrofitClientInstance
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.SphericalUtil
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class PlaceViewModel : ViewModel() {

    var placeList: MutableLiveData<ArrayList<Place>> = MutableLiveData()
    val isLoading: MutableLiveData<Boolean> = MutableLiveData()
    private val service: ApiManager? = RetrofitClientInstance.getRetrofitInstance()?.create(ApiManager::class.java)
    private val mapService: ApiManager? = RetrofitClientInstance.getRetrofitInstanceMap()?.create(
        ApiManager::class.java)
    var currentLatitude = 0.0
    var currentLongitude = 0.0


    fun updatePlaceList(gpsTracker: GPSTracker){
        GlobalScope.launch {
            service?.let {
                currentLatitude = gpsTracker.getLatitude()
                currentLongitude = gpsTracker.getLongitude()
                isLoading.postValue(true)
                val call: Call<List<Place>> = it.getAllPlaces()
                call.enqueue(object : Callback<List<Place>> {
                    override fun onResponse(call: Call<List<Place>>, response: Response<List<Place>>) {
                        if (response.isSuccessful) {
                            val places = response.body()
                            getDistanceAndDuration(places)
                        }
                    }

                    override fun onFailure(call: Call<List<Place>>, t: Throwable) {
                        val e = t
                    }
                })
            }
        }
    }

    fun getDistanceAndDuration(places: List<Place>?){
        GlobalScope.launch {
            mapService?.let { service ->
                places?.forEach { currentPlace ->
                    val call: Call<MyGoogleResponse> = service.getDirections(
                        "json?origin=" + currentLatitude +"," + currentLongitude +
                                "&destination=" + currentPlace.latitude + "," + currentPlace.longitude+ "&mode=driving&key=AIzaSyDrd-ZB__ToWKFrhFvfdzmDOsxiYniskgM")
                    call.enqueue(object : Callback<MyGoogleResponse> {
                        override fun onResponse(call: Call<MyGoogleResponse>, response: Response<MyGoogleResponse>) {
                            if (response.isSuccessful) {
                                val json = response.body()
                                currentPlace.duration = json?.route?.get(0)?.leg?.get(0)?.duration?.text.toString()
                                currentPlace.distanceText = json?.route?.get(0)?.leg?.get(0)?.distance?.text.toString()
                                val distance = currentPlace.distanceText.split(" ")
                                currentPlace.distance = distance[0].replace(",", "").toDouble()
                                if (currentPlace == places.last()) {
                                    sortList(places)
                                }
                            }
                        }

                        override fun onFailure(call: Call<MyGoogleResponse>, t: Throwable) {
                            val error = t
                        }
                    })
                }
            }
        }
    }

    private fun sortList(places: List<Place>?){
        GlobalScope.launch {
            Collections.sort(places) { lhs, rhs ->
                when {
                    lhs.distance.compareTo(rhs.distance) < 0 -> {
                        -1
                    }
                    lhs.distance.compareTo(rhs.distance) > 0 -> {
                        1
                    }
                    else -> {
                        0
                    }
                }
            }
            placeList.postValue(places as ArrayList<Place>)
            isLoading.postValue(false)
        }
    }
}