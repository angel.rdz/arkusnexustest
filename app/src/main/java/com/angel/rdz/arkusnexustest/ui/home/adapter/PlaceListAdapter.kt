package com.angel.rdz.arkusnexustest.ui.home.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.angel.rdz.arkusnexustest.ui.home.model.Place
import com.angel.rdz.arkusnexustest.ui.home.interfaces.PlaceInterface
import com.angel.rdz.arkusnexustest.R
import com.angel.rdz.arkusnexustest.utils.GPSTracker
import com.bumptech.glide.Glide
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.SphericalUtil
import kotlinx.android.synthetic.main.place_item_list.view.*

class PlaceListAdapter(var placeList: ArrayList<Place>) : RecyclerView.Adapter<PlaceListAdapter.MyViewHolder>() {

    lateinit var listener: PlaceInterface

    class MyViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        private var placeName = v.place_name_tv
        private var placeThumb = v.thumbnail_iv
        private var placeRate = v.stars_rb
        private var distanceTv = v.distance_tv
        private var addressLine1 = v.address_line1_tv
        private var addressLine2 = v.address_line2_tv
        private var isPetFriendlyIv = v.pet_friendly_iv
        private var isPetFriendlyTv = v.pet_friendly_label
        private var context = v.context

        fun bind(place: Place) {
            placeName.text = place.placeName
            placeRate.rating = place.rating.toFloat()
            addressLine1.text = place.addressLine1
            addressLine2.text = place.addressLine2
            if (!place.isPetFriendly) {
                isPetFriendlyIv.visibility = View.GONE
                isPetFriendlyTv.visibility = View.GONE
            } else {
                isPetFriendlyIv.visibility = View.VISIBLE
                isPetFriendlyTv.visibility = View.VISIBLE
            }
            Glide
                .with(context)
                .load(place.thumbnail)
                .centerCrop()
                .placeholder(R.mipmap.ic_launcher)
                .into(placeThumb)
            //val gpsTracker = GPSTracker(context)
            distanceTv.text = place.distanceText
            //distanceTv.text = String.format("%.1f km", SphericalUtil.computeDistanceBetween(LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude()), LatLng(place.latitude, place.longitude))/1000)

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.place_item_list, parent, false)
        return MyViewHolder(
            v
        )
    }

    override fun getItemCount(): Int {
        return placeList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(placeList[position])
        holder.itemView.setOnClickListener {
            listener.showPlaceDetail(placeList[position])
        }
    }
}