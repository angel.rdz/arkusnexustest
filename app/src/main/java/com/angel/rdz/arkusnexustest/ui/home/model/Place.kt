package com.angel.rdz.arkusnexustest.ui.home.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Place(@SerializedName("PlaceId")  var placeId: String,
            @SerializedName("PlaceName")  var placeName: String,
            @SerializedName("Address")  var address: String,
            @SerializedName("Category")  var category: String,
            @SerializedName("IsOpenNow")  var isOpenNow: String,
            @SerializedName("Latitude")  var latitude: Double,
            @SerializedName("Longitude")  var longitude: Double,
            @SerializedName("Thumbnail")  var thumbnail: String,
            @SerializedName("Rating")  var rating: Double,
            @SerializedName("IsPetFriendly")  var isPetFriendly: Boolean,
            @SerializedName("AddressLine1")  var addressLine1: String,
            @SerializedName("AddressLine2")  var addressLine2: String,
            @SerializedName("PhoneNumber")  var phoneNumber: String,
            @SerializedName("Site")  var site: String) : Serializable{
    var distance: Double = 0.0
    var distanceText: String = ""
    var duration: String = ""
}