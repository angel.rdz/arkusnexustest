package com.angel.rdz.arkusnexustest.ui.home
import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.angel.rdz.arkusnexustest.*
import com.angel.rdz.arkusnexustest.ui.home.adapter.PlaceListAdapter
import com.angel.rdz.arkusnexustest.ui.home.interfaces.PlaceInterface
import com.angel.rdz.arkusnexustest.ui.home.model.Place
import com.angel.rdz.arkusnexustest.ui.placeDetail.PlaceDetailActivity
import com.angel.rdz.arkusnexustest.utils.GPSTracker
import kotlinx.android.synthetic.main.activity_main.*
import java.io.Serializable


class HomeActivity : AppCompatActivity(), PlaceInterface {

    private lateinit var placeViewModel: PlaceViewModel
    private lateinit var gpsTracker: GPSTracker

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        placeViewModel = ViewModelProvider(this).get(PlaceViewModel::class.java)
        placeViewModel.placeList.observe(this, Observer {
            generateDataList(it)
        })
        placeViewModel.isLoading.observe(this, Observer {
            if (it) {
                showProgress()
            } else {
                hideProgress()
            }
        })

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_REQUEST_CODE)
            }
        } else {
            gpsTracker = GPSTracker(this)
            placeViewModel.updatePlaceList(gpsTracker)
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            LOCATION_REQUEST_CODE -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    gpsTracker = GPSTracker(this)
                    placeViewModel.updatePlaceList(gpsTracker)
                }
                return
            }
            else -> { }
        }
    }

    private fun generateDataList(placeList: ArrayList<Place>) {
        val adapter =
            PlaceListAdapter(
                placeList
            )
        adapter.listener = this
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this@HomeActivity)
        my_recycler_view.layoutManager = layoutManager
        val dividerItemDecoration = DividerItemDecoration(applicationContext, (layoutManager as LinearLayoutManager).orientation)
        my_recycler_view.addItemDecoration(dividerItemDecoration)
        my_recycler_view.adapter = adapter
    }

    override fun showPlaceDetail(place: Place) {
        val intent = Intent(applicationContext, PlaceDetailActivity::class.java)
        intent.putExtra("place", place)
        startActivity(intent)
    }

    fun showProgress(){
        progressBar.visibility = View.VISIBLE
        progressBarBack.visibility = View.VISIBLE
    }

    fun hideProgress(){
        progressBar.visibility = View.GONE
        progressBarBack.visibility = View.GONE
    }

    companion object {
        const val LOCATION_REQUEST_CODE = 1234
    }

}
