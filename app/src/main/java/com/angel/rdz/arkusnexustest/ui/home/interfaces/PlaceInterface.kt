package com.angel.rdz.arkusnexustest.ui.home.interfaces

import com.angel.rdz.arkusnexustest.ui.home.model.Place

interface PlaceInterface {
    fun showPlaceDetail(place: Place)
}