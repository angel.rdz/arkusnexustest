package com.angel.rdz.arkusnexustest.utils

import com.angel.rdz.arkusnexustest.ui.home.model.MyGoogleResponse
import com.angel.rdz.arkusnexustest.ui.home.model.Place
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.HTTP
import retrofit2.http.Path
import retrofit2.http.Url

interface ApiManager {

    @GET("v2/5bf3ce193100008900619966")
    fun getAllPlaces(): Call<List<Place>>

    @GET
    fun getDirections(@Url url: String): Call<MyGoogleResponse>
}