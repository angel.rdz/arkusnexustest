package com.angel.rdz.arkusnexustest.ui.home.model

import com.google.gson.annotations.SerializedName


class MyGoogleResponse(@SerializedName("routes") var route: List<Route>)

class Route(@SerializedName("legs") var leg : List<Leg>)

class Leg(@SerializedName("distance") var distance: Distance, @SerializedName("duration") var duration: Duration)

class Distance(@SerializedName("text") var text: String)

class Duration(@SerializedName("text") var text: String)

