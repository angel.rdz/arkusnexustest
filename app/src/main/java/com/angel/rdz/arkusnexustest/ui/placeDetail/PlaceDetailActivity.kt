package com.angel.rdz.arkusnexustest.ui.placeDetail

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import com.angel.rdz.arkusnexustest.R
import com.angel.rdz.arkusnexustest.ui.home.model.Place
import com.angel.rdz.arkusnexustest.utils.GPSTracker
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_place_detail.*

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class PlaceDetailActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var gpstracker : GPSTracker
    private var currentLatitude = 0.0
    private var currentLongitude = 0.0
    private lateinit var currentPlace: Place

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_place_detail)
        gpstracker = GPSTracker(this)
        currentLatitude = gpstracker.getLatitude()
        currentLongitude = gpstracker.getLongitude()
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        currentPlace = intent.getSerializableExtra("place") as Place
        place_name_tv.text = currentPlace.placeName
        stars_rb.rating = currentPlace.rating.toFloat()
        address_line1_tv.text = currentPlace.addressLine1
        address_line2_tv.text = currentPlace.addressLine2
        distance_tv.text = currentPlace.distanceText
        time_label.text = currentPlace.duration
        if (currentPlace.isPetFriendly) {
            pet_friendly_iv.visibility = View.VISIBLE
        } else {
            pet_friendly_iv.visibility = View.GONE
        }

        directions_layout.setOnClickListener {
            showGoogleMapIntent()
        }

        number_label.text = currentPlace.phoneNumber
        call_layout.setOnClickListener {
            showDialIntent()
        }

        wbsite_label.text = currentPlace.site
        website_layout.setOnClickListener {
            showWebView()
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        val placeLocation = LatLng(currentPlace.latitude, currentPlace.longitude)
        mMap.addMarker(MarkerOptions()
            .position(placeLocation)
            .title(currentPlace.placeName)
            .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker)))
        //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(placeLocation , 15.2f))
        val cameraUpdate = CameraUpdateFactory.newLatLngZoom(placeLocation , 15.2f)
        mMap.animateCamera(cameraUpdate)
    }

    private fun showWebView() {
        val builder = CustomTabsIntent.Builder()
        builder.setCloseButtonIcon(getDrawable(R.drawable.ic_arrow_back_white).toBitmap())
        builder.setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary))
        val customTabsIntent = builder.build()
        customTabsIntent.launchUrl(this, Uri.parse(currentPlace.site))
    }

    private fun showDialIntent(){
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:" + currentPlace.phoneNumber)
        startActivity(intent)
    }

    private fun showGoogleMapIntent(){
        val urlAddress = "https://www.google.com/maps/dir/" +
                "?api=1&origin="+currentLatitude+","+currentLongitude+"&" +
                "destination="+currentPlace.latitude+","+currentPlace.longitude+"&" +
                "travelmode=driving"
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(urlAddress))
        startActivity(intent)
    }

}
